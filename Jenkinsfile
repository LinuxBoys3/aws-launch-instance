#!/usr/bin/env groovy
pipeline {

  agent any


  parameters {
    booleanParam(name: 'destroy', description: 'destroy the stack', defaultValue: false)
    string(name: 'tf_version', description: 'Terraform version to install', defaultValue: '0.10.2')
    choice(name: 'commons_acct', description: 'Select a Commons account', choices: 'abhishek481')
    choice(name: 'commons_region', description: 'Select a Commons region', choices: 'us-east-1a\nus-west-2b')
    choice(name: 'app_environment', description: 'Select an application environment', choices: 'sbx\ndev\nqa\nprd')
  }

  environment {
    BUILD_TYPE = "aws"
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '20'))
    disableConcurrentBuilds()
  }

  stages {
    stage('Retrieve shared libraries repo') {
      steps {
        dir("shared-libs") {
          script {
            git([
              url: 'git@bitbucket.org:LinuxBoys3/shared-jenkins-repo.git',
              credentialsId: '5b276196-8637-4cc5-a784-44cf48bdefa0',
              branch: 'master'
            ])
          }
        }
      }
    }

    stage( 'Set up the work environment') {
     steps {
          sh """
            if [ -z "${env.JOB_BASE_NAME}" ]; then echo "JOB_BASE_NAME not set"; exit 1; else echo "JOB_BASE_NAME=${JOB_BASE_NAME}"; fi
            /bin/cd ${WORKSPACE}/${params.commons_region}/${params.app_environment}
            /bin/cat ${WORKSPACE}/shared-libs/resources/accounts/${params.commons_acct}/${params.commons_region}/app/_parameters.groovy >> parameters.groovy
            /bin/cat ${WORKSPACE}/shared-libs/resources/accounts/${params.commons_acct}/${params.commons_region}/app/_aws_params.groovy >> aws_params.groovy
            /bin/cat ${WORKSPACE}/shared-libs/resources/accounts/${params.commons_acct}/${params.commons_region}/app/_terraform.tfvars >> terraform.tfvars
            /bin/cat ${WORKSPACE}/shared-libs/resources/accounts/${params.commons_acct}/${params.commons_region}/app/_backend.config >> backend.config
        """
         load "${params.commons_region}/${params.app_environment}/parameters.groovy"
         load "${params.commons_region}/${params.app_environment}/aws_params.groovy"
       }
     }

      stage('Ensure Terraform is present') {
          steps {
              sh """
             ${WORKSPACE}/shared-libs/resources/get_terraform.sh --version '${params.tf_version}'
           """
          }
      }

      stage('Terraform plan instance') {
          when {
              expression { !params.destroy }
          }
          steps {
              ansiColor('xterm') {
                  echo "Planning instance in ... ${AWS_ACCOUNT_NAME} / ${AWS_REGION}"
                  sh """
               set -x
               source /usr/local/bin/temp_creds.sh ${AWS_ACCOUNT_ID} ${AWS_REGION} ${BUILD_ROLE}
               /opt/terraform/${params.tf_version}/terraform init -backend-config=${params.commons_region}/${params.app_environment}/backend.config ${params.commons_region}/${params.app_environment}
               /opt/terraform/${params.tf_version}/terraform plan -out=${params.commons_region}/${params.app_environment}/terraform.plan -var-file=${params.commons_region}/${params.app_environment}/terraform.tfvars ${params.commons_region}/${params.app_environment}
             """
              }
          }
      }

      stage("Validate before Apply") {
          steps {
              timeout(time:30, unit:'MINUTES') {
                  input 'Are you sure? Review the output of the previous step before proceeding!'
              }
          }
      }

      stage('TerraDeploy the objects') {
          when {
              expression { !params.destroy }
          }
          steps {
              ansiColor('xterm') {
                  echo "Building the objects"
                  sh """
            source /usr/local/bin/temp_creds.sh ${AWS_ACCOUNT_ID} ${AWS_REGION} ${BUILD_ROLE}
            set -x
            /opt/terraform/${params.tf_version}/terraform apply ${params.commons_region}/${params.app_environment}/terraform.plan
          """
              }
          }
      }

      stage('TerraDestroy') {
          when {
              expression { params.destroy }
          }
          steps {
              ansiColor('xterm') {
                  echo "destroy the objects"
                  sh """
            set -x
            source /usr/local/bin/temp_creds.sh ${AWS_ACCOUNT_ID} ${AWS_REGION} ${BUILD_ROLE}
            /opt/terraform/${params.tf_version}/terraform init -backend-config=${params.commons_region}/${params.app_environment}/backend.config ${params.commons_region}/${params.app_environment}
            /opt/terraform/${params.tf_version}/terraform destroy -force -var-file=${params.commons_region}/${params.app_environment}/terraform.tfvars ${params.commons_region}/${params.app_environment}
          """
              }
          }
      }
  }

    post {
        success {
            deleteDir()
        }
    }
}



