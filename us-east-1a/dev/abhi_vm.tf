//variables.tf
variable "abhi_VM_instance_type" {
  description = "AWS instance type (a.k.a. flavor)"
}
variable "abhi_VM_filesystems" {
  description = "Collection of disk/volume/fs strings"
}
variable "abhi_VM_data_vol_sz" {
  description = "Size of disk for the data volume(s)"
}
variable "abhi_VM_puppet_role" {
  description = "Puppet role that host should be configured with."
}

data "template_file" "abhi_VM_cloudinit_storage_script" {
  template = "${file("shared-libs/resources/cloud-init/storage_setup.sh")}"
  vars {
    filesystems = "${var.abhi_VM_filesystems}"
  }
}

data "template_file" "abhi_VM_cloudinit_firstboot" {
  template = "${file("shared-libs/resources/cloud-init/first_boot.conf")}"
  vars {
    app_environment = "${var.app_environment}"
    puppet_role = "${var.abhi_VM_puppet_role}"
    root_email_addr = "${var.root_email_addr}"
    newrelic_key = "${var.newrelic_key}"
  }
}

data "template_cloudinit_config" "abhi_VM_final" {
  gzip = false

  part {
    filename     = "storage_setup.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.abhi_VM_cloudinit_storage_script.rendered}"
  }
  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.cloudinit_hostname.rendered}"
  }
  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.abhi_VM_cloudinit_firstboot.rendered}"
  }
}

// ebs
resource "aws_ebs_volume" "abhi_VM_data" {
  type              = "standard"
  size              = "${var.abhi_VM_data_vol_sz}"
  availability_zone = "${var.aws_avail_zone}"
  encrypted         = "true"
  tags              = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-abhi_VM-vol"))}"
  lifecycle {
    prevent_destroy = true,
  }
}

resource "aws_volume_attachment" "abhi_VM_data" {
  volume_id    = "${aws_ebs_volume.abhi_VM_data.id}"
  instance_id  = "${aws_instance.abhi_VM.id}"
  device_name  = "/dev/sdb"
  skip_destroy = "true"
}
// Set up deploy SSH key
resource "aws_key_pair" "unix_abhi_VM_ssh_key" {
  key_name   = "${var.tenant}_${var.app_environment}_abhi_VM_ssh_key"
  public_key = "${file("/home/ec2-user/.ssh/deployer_key_rsa.pub")}"
}

// Create the VM instance
// You MUST have an instance profile that starts with your namespace attached to your instance
resource "aws_instance" "abhi_VM" {
  iam_instance_profile   = "${var.tenant}-instance-profile"
  ami                    = "${data.aws_ami.unixeng_ami.id}"
  instance_type          = "${var.abhi_VM_instance_type}"
  key_name               = "${aws_key_pair.unix_abhi_VM_ssh_key.key_name}"
  subnet_id              = "${var.subnet_id}"
  vpc_security_group_ids = ["${var.security_group_id}", "${aws_security_group.unix_internal_sg.id}"]
  tags                   = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-abhi_VM"))}"
  user_data              = "${data.template_cloudinit_config.abhi_VM_final.rendered}"
  lifecycle {
    ignore_changes = ["ami", "iam_instance_profile", "instance_type", "key_name", "subnet_id", "user_data"]
  }
}

// Create a DNS record (optional)
resource "aws_route53_record" "abhi_VM" {
  zone_id = "${var.zone_id}"
  name    = "ip-${replace(aws_instance.abhi_VM.private_ip, ".", "-")}"
  type    = "A"
  ttl     = "1800"
  records = ["${aws_instance.abhi_VM.private_ip}"]
}

// Outputs.tf
output "abhi_VM_ipaddress" {
  value = "${aws_instance.abhi_VM.private_ip}"
}

output "abhi_VM_fqdn" {
  value = "${aws_route53_record.abhi_VM.fqdn}"
}
