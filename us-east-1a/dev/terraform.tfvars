app_environment="dev"
aws_avail_zone="us-east-1a"
security_group_id="sg-33b13a44"
stack_name="aws-testVM"

# Viya Server
abhi_VM_instance_type="t2.micro"
abhi_VM_data_vol_sz="10"
abhi_VM_filesystems="b:data_vg:lv_test_vm:100%FREE:/testmount:xfs:auto"
abhi_VM_puppet_role="role::bi::aws-testvm"

tags={
  "Environment"="Non"
  "AppEnvironment"="dev"
  "Requestor"="abhishekreddy.karra@gmail.com"
  "Department"="Testing"
  "AppId"="none"
  "AppName"="aws-testVM"
  "CostCenter"="00000"
  "ProjectCode"="KLO"
  "DataClass"="ru"
}
