//variables.tf
variable "tags" {
  description = "A map of tags to add to all resources"
  default     = {}
  type        = "map"
}
variable "account_id" {
  description = "AWS account id"
}
variable "app_environment" {
  description = "Application environment (usually sbx, dev, qa, stg, prd, etc.)"
}
variable "aws_avail_zone" {
  description = "AWS availability zone, which includes the region and the availability zone letter (i.e. us-west-2b)"
}
variable "zone_id" {
  description = "route 53 tldn zone id"
}
variable "subnet_id" {
  description = "which subnet to put the thing, or so help me"
}
variable "vpc_id" {
  description = "which vpc to put the thing, or so help me"
}
variable "security_group_id" {
  description = "Security group ID to use for instances"
}
variable "tenant" {
  description = "which vpc to put the thing, or so help me"
  default     = "unix"
}
variable "source_ami_owners" {
  description = "Which accounts to search for the source AMI. Defaults are abhishek481."
  default     = ["426110107237"]
  type        = "list"
}
variable "root_email_addr" {
  description = "Email address that root mail should be forwarded to"
  default     = "abhishekreddy.karra@gmail.com"
}
variable "newrelic_key" {
  description = "NewRelic license key string"
  default     = ""
}
variable "stack_name" {
  description = "Stack name." 
}
variable "target_os" {
  description = "Which OS and version should we make? (Should be the <os-n> part of the AMI name, so if AMI name is unix-centos-7-x86_64-base-master-*, this variable should be set to centos-7"
  default     = "redhat-7"
}

// Get a handle to the latest available AMI
data "aws_ami" "unixeng_ami" {
  most_recent = true
  filter {
    name   = "name"
    values = ["unix-${var.target_os}-x86_64-base-master-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = "${var.source_ami_owners}"
}

// Start custom role setup
data "aws_iam_policy_document" "unix_saslpo_assumeable_policy" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "unix_saslpo_role" {
  name               = "${var.tenant}-${var.stack_name}-role"
  path               = "/"
  assume_role_policy = "${data.aws_iam_policy_document.unix_saslpo_assumeable_policy.json}"
}

resource "aws_iam_role_policy_attachment" "unix_saslpo_policy_attachment" {
  role       = "${aws_iam_role.unix_saslpo_role.name}"
  policy_arn = "arn:aws:iam::${var.account_id}:policy/unix-sas-cdtlpoabhi_VM-custom-policy"
}

resource "aws_iam_instance_profile" "unix_saslpo_instance_profile" {
  name = "${var.tenant}-${var.stack_name}-instance-profile"
  role = "${aws_iam_role.unix_saslpo_role.name}"
}
// End Role setup

// Start IAM user for cross-account S3 access
resource "aws_iam_user" "xacct_user" {
  name = "s.${var.tenant}-${var.stack_name}-${var.app_environment}-xacct"
}

resource "aws_iam_user_policy_attachment" "xacct-attachment" {
  user       = "${aws_iam_user.xacct_user.name}"
  policy_arn = "arn:aws:iam::${var.account_id}:policy/unix-sas-cdtlpoabhi_VM-custom-policy"
}
// End IAM user for cross-account S3 access


// Get a handle to the zone, so we can dynamically determine domain
data "aws_route53_zone" "myzone" {
  zone_id = "${var.zone_id}"
}

// Set up cloud-config (cloud-init):
data "template_file" "cloudinit_hostname" {
  template = "${file("shared-libs/resources/cloud-init/hostname.sh")}"
  vars {
    domain = "${data.aws_route53_zone.myzone.name}"
  }
}

resource "aws_security_group" "unix_internal_sg" {
  name        = "${var.stack_name}-${var.app_environment}_unix_internal_sg"
  description = "${var.stack_name}-${var.app_environment} unix internal sg"
  vpc_id      = "${var.vpc_id}"
  tags        = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-internal_sg"))}"
  ingress {
    from_port  = "0"
    to_port    = "0"
    protocol   = "-1"
    self       = "true"
  }
  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }
  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }
  ingress {
    from_port   = "5570"
    to_port     = "5570"
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }
  ingress {
    from_port   = "17511"
    to_port     = "17511"
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }
  ingress {
    from_port   = "17541"
    to_port     = "17541"
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }
  ingress {
    from_port   = "17551"
    to_port     = "17551"
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }
  ingress {
    from_port   = "8777"
    to_port     = "8777"
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }
}
