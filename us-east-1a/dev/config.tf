terraform {
  backend "s3" {
    encrypt = "true"
    key     = "tf_state/unix/akarra-test-dev-us-east-1-dev.tfstate"
    region  = "us-east-1"
    bucket  = "aws-testVM-us-east-1"
    dynamodb_table = "entsvcs-terraform-abhishek481-us-east-1"
  }
}
