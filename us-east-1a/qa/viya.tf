//variables.tf
variable "viya_instance_type" {
  description = "AWS instance type (a.k.a. flavor)"
}
variable "viya_filesystems" {
  description = "Collection of disk/volume/fs strings"
}
variable "viya_data_vol_sz" {
  description = "Size of disk for the data volume(s)"
}
variable "viya_efs_fsid" {
  description = "The EFS fs-id for the volume to be mounted (used by mount-target)"
}
variable "viya_efs_vol_mountpoint" {
  description = "The mountpoint for the EFS volume"
}
variable "viya_puppet_role" {
  description = "Puppet role that host should be configured with."
}


data "template_file" "viya_cloudinit_storage_script" {
  template = "${file("shared-libs/resources/cloud-init/storage_setup.sh")}"
  vars {
    filesystems = "${var.viya_filesystems}"
  }
}

  data "template_file" "viya_cloudinit_efs_vols" {
    template = "${file("shared-libs/resources/cloud-init/efs_mounts.sh")}"
    vars {
      efs_vol_mount = "/backupvault"
      efs_vol_ip = "10.180.105.77"
    }
  }


data "template_file" "viya_cloudinit_firstboot" {
  template = "${file("shared-libs/resources/cloud-init/first_boot.conf")}"
  vars {
    app_environment = "${var.app_environment}"
    puppet_role = "${var.viya_puppet_role}"
    root_email_addr = "${var.root_email_addr}"
    newrelic_key = "${var.newrelic_key}"
  }
}

data "template_cloudinit_config" "viya_final" {
  gzip = false

  part {
    filename     = "storage_setup.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.viya_cloudinit_storage_script.rendered}"
  }
  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.cloudinit_hostname.rendered}"
  }
  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.viya_cloudinit_firstboot.rendered}"
  }
  part {
    filename     = "zz_efs_mounts.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.viya_cloudinit_efs_vols.rendered}"
  }
}

// ebs
resource "aws_ebs_volume" "viya_data" {
  type              = "standard"
  size              = "${var.viya_data_vol_sz}"
  availability_zone = "${var.aws_avail_zone}"
  encrypted         = "true"
  tags              = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-viya-vol"))}"
  lifecycle {
    prevent_destroy = false,
  }
}

resource "aws_volume_attachment" "viya_data" {
  volume_id    = "${aws_ebs_volume.viya_data.id}"
  instance_id  = "${aws_instance.viya.id}"
  device_name  = "/dev/sdb"
  skip_destroy = "true"
}


// Set up deploy SSH key
resource "aws_key_pair" "unix_viya_ssh_key" {
  key_name   = "${var.tenant}_${var.stack_name}_${var.app_environment}_viya_ssh_key"
  public_key = "${file("/home/ec2-user/.ssh/deployer_key_rsa.pub")}"
}

// Create the VM instance
// You MUST have an instance profile that starts with your namespace attached to your instance
resource "aws_instance" "viya" {
  iam_instance_profile   = "${var.tenant}-${var.stack_name}-${var.app_environment}-instance-profile"
  ami                    = "${data.aws_ami.unixeng_ami.id}"
  instance_type          = "${var.viya_instance_type}"
  key_name               = "${aws_key_pair.unix_viya_ssh_key.key_name}"
  subnet_id              = "${var.subnet_id}"
  vpc_security_group_ids = ["${var.security_group_id}", "${aws_security_group.unix_internal_sg.id}","${aws_security_group.sasviya_efs_sg.id}"]
  tags                   = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-viya"))}"
  user_data              = "${data.template_cloudinit_config.viya_final.rendered}"
  lifecycle {
    ignore_changes = ["ami", "iam_instance_profile", "instance_type", "key_name", "subnet_id", "user_data"]
  }
}

// Create a DNS record (optional)
resource "aws_route53_record" "viya" {
  zone_id = "${var.zone_id}"
  name    = "ip-${replace(aws_instance.viya.private_ip, ".", "-")}"
  type    = "A"
  ttl     = "1800"
  records = ["${aws_instance.viya.private_ip}"]
}

// Outputs.tf
output "viya_ipaddress" {
  value = "${aws_instance.viya.private_ip}"
}

output "viya_fqdn" {
  value = "${aws_route53_record.viya.fqdn}"
}
