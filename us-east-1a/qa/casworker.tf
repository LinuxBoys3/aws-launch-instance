//variables.tf
variable "casworker_instance_type" {
  description = "AWS instance type (a.k.a. flavor)"
}
variable "casworker_filesystems" {
  description = "Collection of disk/volume/fs strings"
}
variable "casworker_sas_vol_sz" {
  description = "Size of disk for the sas volume(s)"
}
variable "casworker_cache_vol_sz" {
  description = "Size of disk for the cache volume(s)"
}
variable "casworker_efs_fsid" {
  description = "The EFS fs-id for the volume to be mounted (used by mount-target)"
}
variable "casworker_efs_vol_mountpoint" {
  description = "The mountpoint for the EFS volume"
}
variable "casworker_puppet_role" {
  description = "Puppet role that host should be configured with."
}
variable "casworker_instance_count" {
  description = "Number of instances"
}

data "template_file" "casworker_cloudinit_storage_script" {
  template = "${file("shared-libs/resources/cloud-init/storage_setup.sh")}"
  vars {
    filesystems = "${var.casworker_filesystems}"
  }
}

data "template_file" "casworker_cloudinit_efs_vols" {
  template = "${file("shared-libs/resources/cloud-init/efs_mounts.sh")}"
  vars {
    efs_vol_mount = "/backupvault"
    efs_vol_ip = "10.180.105.77"
  }
}

data "template_file" "casworker_cloudinit_firstboot" {
  template = "${file("shared-libs/resources/cloud-init/first_boot.conf")}"
  vars {
    app_environment = "${var.app_environment}"
    puppet_role = "${var.casworker_puppet_role}"
    root_email_addr = "${var.root_email_addr}"
    newrelic_key = "${var.newrelic_key}"
  }
}

data "template_cloudinit_config" "casworker_final" {
  gzip = false

  part {
    filename     = "storage_setup.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.casworker_cloudinit_storage_script.rendered}"
  }
  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.cloudinit_hostname.rendered}"
  }
  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.casworker_cloudinit_firstboot.rendered}"
  }
 part {
    filename     = "zz_efs_mounts.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.casworker_cloudinit_efs_vols.rendered}"
  }
}


// ebs
resource "aws_ebs_volume" "casworker_sas" {
  type              = "standard"
  size              = "${var.casworker_sas_vol_sz}"
  availability_zone = "${var.aws_avail_zone}"
  encrypted         = "true"
  tags              = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-casworker-vol"))}"
  count             = "${var.casworker_instance_count}"
  lifecycle {
    prevent_destroy = false,
  }
}

resource "aws_volume_attachment" "casworker_sas" {
  volume_id    = "${element(aws_ebs_volume.casworker_sas.*.id, count.index)}"
  instance_id  = "${element(aws_instance.casworker.*.id, count.index)}"
  device_name  = "/dev/sdb"
  skip_destroy = "true"
  count        = "${var.casworker_instance_count}"
}
resource "aws_ebs_volume" "casworker_cache" {
  type              = "st1"
  size              = "${var.casworker_cache_vol_sz}"
  availability_zone = "${var.aws_avail_zone}"
  encrypted         = "true"
  tags              = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-casworker-vol"))}"
  count             = "${var.casworker_instance_count}" 
  lifecycle {
    prevent_destroy = false,
  }
}

resource "aws_volume_attachment" "casworker_cache" {
  volume_id    = "${element(aws_ebs_volume.casworker_cache.*.id, count.index)}"
  instance_id  = "${element(aws_instance.casworker.*.id, count.index)}"
  device_name  = "/dev/sdc"
  skip_destroy = "true"
  count        = "${var.casworker_instance_count}"
}

// Set up deploy SSH key
resource "aws_key_pair" "unix_casworker_ssh_key" {
  key_name   = "${var.tenant}_${var.stack_name}_${var.app_environment}_casworker_ssh_key"
  public_key = "${file("/home/ec2-user/.ssh/deployer_key_rsa.pub")}"
}

// Create the VM instance
// You MUST have an instance profile that starts with your namespace attached to your instance
resource "aws_instance" "casworker" {
  iam_instance_profile   = "${var.tenant}-${var.stack_name}-${var.app_environment}-instance-profile"
  ami                    = "${data.aws_ami.unixeng_ami.id}"
  instance_type          = "${var.casworker_instance_type}"
  key_name               = "${aws_key_pair.unix_casworker_ssh_key.key_name}"
  subnet_id              = "${var.subnet_id}"
  vpc_security_group_ids = ["${var.security_group_id}", "${aws_security_group.unix_internal_sg.id}","${aws_security_group.sasviya_efs_sg.id}"]
  tags                   = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-casworker.${count.index}"))}"
  user_data              = "${data.template_cloudinit_config.casworker_final.rendered}"
  count                  = "${var.casworker_instance_count}"
  lifecycle {
    ignore_changes = ["ami", "iam_instance_profile", "instance_type", "key_name", "subnet_id", "user_data"]
  }
}

// Create a DNS record (optional)
resource "aws_route53_record" "casworker" {
  zone_id = "${var.zone_id}"
  name    = "ip-${replace("${element(aws_instance.casworker.*.private_ip, count.index)}", ".", "-")}"
  type    = "A"
  ttl     = "1800"
  records = ["${element(aws_instance.casworker.*.private_ip, count.index)}"]
  count   = "${var.casworker_instance_count}"
}

// Outputs.tf
// use the * when you are building more than one server (instance_count > 1)
output "casworker_ipaddress" {
  value = ["${aws_instance.casworker.*.private_ip}"]
}
output "casworker_fqdn" {
  value = ["${aws_route53_record.casworker.*.fqdn}"]
}
