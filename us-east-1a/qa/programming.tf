//variables.tf
variable "programming_instance_type" {
  description = "AWS instance type (a.k.a. flavor)"
}
variable "programming_filesystems" {
  description = "Collection of disk/volume/fs strings"
}
variable "programming_data_vol_sz" {
  description = "Size of disk for the data volume(s)"
}
variable "programming_hiio_vol_sz" {
  description = "Size of disk for the hiio volume(s)"
}
variable "programming_hiio_iops" {
  description = "Number of IO operations for the hiio volume(s)"
}
variable "programming_hiio_type" {
  description = "EBS type for the hiio volume(s)"
}
variable "programming_sas_vol_sz" {
  description = "Size of disk for the sas volume(s)"
}
variable "programming_efs_fsid" {
  description = "The EFS fs-id for the volume to be mounted (used by mount-target)"
}
variable "programming_efs_vol_mountpoint" {
  description = "The mountpoint for the EFS volume"
}
variable "programming_puppet_role" {
  description = "Puppet role that host should be configured with."
}

data "template_file" "programming_cloudinit_storage_script" {
  template = "${file("shared-libs/resources/cloud-init/storage_setup.sh")}"
  vars {
    filesystems = "${var.programming_filesystems}"
  }
}
data "template_file" "programming_cloudinit_efs_vols" {
  template = "${file("shared-libs/resources/cloud-init/efs_mounts.sh")}"
  vars {
    efs_vol_mount = "/backupvault"
    efs_vol_ip = "10.180.105.77"
  }
}
data "template_file" "programming_cloudinit_firstboot" {
  template = "${file("shared-libs/resources/cloud-init/first_boot.conf")}"
  vars {
    app_environment = "${var.app_environment}"
    puppet_role = "${var.programming_puppet_role}"
    root_email_addr = "${var.root_email_addr}"
    newrelic_key = "${var.newrelic_key}"
  }
}

data "template_cloudinit_config" "programming_final" {
  gzip = false

  part {
    filename     = "storage_setup.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.programming_cloudinit_storage_script.rendered}"
  }
  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.cloudinit_hostname.rendered}"
  }
  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.programming_cloudinit_firstboot.rendered}"
  }
  part {
    filename     = "zz_efs_mounts.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.programming_cloudinit_efs_vols.rendered}"
  }
}
data "aws_efs_mount_target" "programming_efs_target" {
  mount_target_id = "fsmt-667a022e"
}

// ebs
resource "aws_ebs_volume" "programming_data" {
  type              = "st1"
  size              = "${var.programming_data_vol_sz}"
  availability_zone = "${var.aws_avail_zone}"
  encrypted         = "true"
  tags              = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-programming-vol"))}"
  lifecycle {
    prevent_destroy = false,
  }
}

resource "aws_volume_attachment" "programming_data" {
  volume_id    = "${aws_ebs_volume.programming_data.id}"
  instance_id  = "${aws_instance.programming.id}"
  device_name  = "/dev/sdd"
  skip_destroy = "true"
}

// ebs
resource "aws_ebs_volume" "programming_hiio" {
  type              = "${var.programming_hiio_type}"
  iops              = "${var.programming_hiio_iops}"
  size              = "${var.programming_hiio_vol_sz}"
  availability_zone = "${var.aws_avail_zone}"
  encrypted         = "true"
  tags              = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-programming-vol"))}"
  lifecycle {
    prevent_destroy = false,
  }
}

resource "aws_volume_attachment" "programming_hiio" {
  volume_id    = "${aws_ebs_volume.programming_hiio.id}"
  instance_id  = "${aws_instance.programming.id}"
  device_name  = "/dev/sdc"
  skip_destroy = "true"
}
resource "aws_ebs_volume" "programming_sas" {
  type              = "standard"
  size              = "${var.programming_sas_vol_sz}"
  availability_zone = "${var.aws_avail_zone}"
  encrypted         = "true"
  tags              = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-programming-vol"))}"
  lifecycle {
    prevent_destroy = false,
  }
}

resource "aws_volume_attachment" "programming_sas" {
  volume_id    = "${aws_ebs_volume.programming_sas.id}"
  instance_id  = "${aws_instance.programming.id}"
  device_name  = "/dev/sdb"
  skip_destroy = "true"
}

resource "aws_efs_mount_target" "programming_efs_target" {
  file_system_id  = "fs-fb365fb3"
  subnet_id       = "${var.subnet_id}"
  security_groups = ["${aws_security_group.sasviya_efs_sg.id}"]
}

// Set up deploy SSH key
resource "aws_key_pair" "unix_programming_ssh_key" {
  key_name   = "${var.tenant}_${var.stack_name}_${var.app_environment}_programming_ssh_key"
  public_key = "${file("/home/ec2-user/.ssh/deployer_key_rsa.pub")}"
}

// Create the VM instance
// You MUST have an instance profile that starts with your namespace attached to your instance
resource "aws_instance" "programming" {
  iam_instance_profile   = "${var.tenant}-${var.stack_name}-${var.app_environment}-instance-profile"
  ami                    = "${data.aws_ami.unixeng_ami.id}"
  instance_type          = "${var.programming_instance_type}"
  key_name               = "${aws_key_pair.unix_programming_ssh_key.key_name}"
  subnet_id              = "${var.subnet_id}"
  vpc_security_group_ids = ["${var.security_group_id}", "${aws_security_group.unix_internal_sg.id}","${aws_security_group.sasviya_efs_sg.id}"]
  tags                   = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-programming"))}"
  user_data              = "${data.template_cloudinit_config.programming_final.rendered}"
  lifecycle {
    ignore_changes = ["ami", "iam_instance_profile", "instance_type", "key_name", "subnet_id", "user_data"]
  }
}

// Create a DNS record (optional)
resource "aws_route53_record" "programming" {
  zone_id = "${var.zone_id}"
  name    = "ip-${replace(aws_instance.programming.private_ip, ".", "-")}"
  type    = "A"
  ttl     = "1800"
  records = ["${aws_instance.programming.private_ip}"]
}

// Outputs.tf
output "programming_ipaddress" {
  value = "${aws_instance.programming.private_ip}"
}
output "programming_dnsname" {
  value = "${aws_route53_record.programming.fqdn}"
}
