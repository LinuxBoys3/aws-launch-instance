app_environment="qa"
aws_avail_zone="us-east-1a"
security_group_id="sg-6dd2da27"
stack_name="bi-sas-lpo"

# CASWorker Server
casworker_instance_type="t2.micro"
casworker_sas_vol_sz="10"
casworker_cache_vol_sz="800"
casworker_filesystems="b:sas_vg:lv_opt_sas:100%FREE:/opt/sas:xfs:auto:p c:cascache_vg:lv_cas_cache:100%FREE:/cascache:xfs:auto:e"
casworker_puppet_role="role::bi::sas_lpo_casworker"
casworker_instance_count="2"
casworker_efs_vol_mountpoint="/backupvault"
casworker_efs_fsid="fs-fb365fb3"

# CASController Server
cascontroller_instance_type="t2.micro"
cascontroller_sas_vol_sz="10"
cascontroller_cache_vol_sz="800"
cascontroller_filesystems="b:sas_vg:lv_opt_sas:100%FREE:/opt/sas:xfs:auto:p c:cascache_vg:lv_cas_cache:100%FREE:/cascache:xfs:auto:e"
cascontroller_puppet_role="role::bi::sas_lpo_casworker"
cascontroller_efs_vol_mountpoint="/backupvault"
cascontroller_efs_fsid="fs-fb365fb3"


# Programming Interface Server
programming_instance_type="t2.micro"
programming_sas_vol_sz="10"
programming_hiio_vol_sz="400"
programming_hiio_iops="2000"
programming_hiio_type="io1"
programming_data_vol_sz="400"
programming_filesystems="b:sas_vg:lv_opt_sas:100%FREE:/opt/sas:xfs:auto:p c:highio_vg:lv_workfs:100%FREE:/saswork:xfs:8m:p d:data_vg:lv_data:100%FREE:/data:xfs:auto:e"
programming_puppet_role="role::bi::sas_lpo_cascontroller"
programming_efs_vol_mountpoint="/backupvault"
programming_efs_fsid="fs-fb365fb3"

# Visual Interface Server
viya_instance_type="t2.micro"
viya_data_vol_sz="10"
viya_filesystems="b:data_vg:lv_opt_sas:100%FREE:/opt/sas:xfs:auto"
viya_puppet_role="role::bi::sas_lpo_viya"
viya_efs_vol_mountpoint="/backupvault"
viya_efs_fsid="fs-fb365fb3"

tags={
  "Environment"="Non"
  "AppEnvironment"="qa"
  "Requestor"="abhishekreddy.karra@gmail.com"
  "Department"="BI-SAS"
  "AppId"="none"
  "AppName"="SAS-LPO"
  "CostCenter"="100676"
  "ProjectCode"="KLO"
  "DataClass"="ru"
}
