//variables.tf
variable "cascontroller_instance_type" {
  description = "AWS instance type (a.k.a. flavor)"
}
variable "cascontroller_filesystems" {
  description = "Collection of disk/volume/fs strings"
}
variable "cascontroller_sas_vol_sz" {
  description = "Size of disk for the sas volume(s)"
}
variable "cascontroller_cache_vol_sz" {
  description = "Size of disk for the cache volume(s)"
}
variable "cascontroller_efs_fsid" {
  description = "The EFS fs-id for the volume to be mounted (used by mount-target)"
}
variable "cascontroller_efs_vol_mountpoint" {
  description = "The mountpoint for the EFS volume"
}
variable "cascontroller_puppet_role" {
  description = "Puppet role that host should be configured with."
}

data "template_file" "cascontroller_cloudinit_storage_script" {
  template = "${file("shared-libs/resources/cloud-init/storage_setup.sh")}"
  vars {
    filesystems = "${var.cascontroller_filesystems}"
  }
}

data "template_file" "cascontroller_cloudinit_efs_vols" {
  template = "${file("shared-libs/resources/cloud-init/efs_mounts.sh")}"
  vars {
    efs_vol_mount = "/backupvault"
    efs_vol_ip    = "10.180.105.77"
  }
}

data "template_file" "cascontroller_cloudinit_firstboot" {
  template = "${file("shared-libs/resources/cloud-init/first_boot.conf")}"
  vars {
    app_environment = "${var.app_environment}"
    puppet_role = "${var.cascontroller_puppet_role}"
    root_email_addr = "${var.root_email_addr}"
    newrelic_key = "${var.newrelic_key}"
  }
}

data "template_cloudinit_config" "cascontroller_final" {
  gzip = false

  part {
    filename     = "storage_setup.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.cascontroller_cloudinit_storage_script.rendered}"
  }
  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.cloudinit_hostname.rendered}"
  }
  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.cascontroller_cloudinit_firstboot.rendered}"
  }
 part {
    filename     = "zz_efs_mounts.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.cascontroller_cloudinit_efs_vols.rendered}"
  }
}

// ebs
resource "aws_ebs_volume" "cascontroller_sas" {
  type              = "standard"
  size              = "${var.cascontroller_sas_vol_sz}"
  availability_zone = "${var.aws_avail_zone}"
  encrypted         = "true"
  tags              = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-cascontroller-vol"))}"
  lifecycle {
    prevent_destroy = false,
  }
}

resource "aws_volume_attachment" "cascontroller_sas" {
  volume_id    = "${aws_ebs_volume.cascontroller_sas.id}"
  instance_id  = "${aws_instance.cascontroller.id}"
  device_name  = "/dev/sdb"
  skip_destroy = "true"
}
resource "aws_ebs_volume" "cascontroller_cache" {
  type              = "st1"
  size              = "${var.cascontroller_cache_vol_sz}"
  availability_zone = "${var.aws_avail_zone}"
  encrypted         = "true"
  tags              = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-cascontroller-vol"))}"
  lifecycle {
    prevent_destroy = false,
  }
}

resource "aws_volume_attachment" "cascontroller_cache" {
  volume_id    = "${aws_ebs_volume.cascontroller_cache.id}"
  instance_id  = "${aws_instance.cascontroller.id}"
  device_name  = "/dev/sdc"
  skip_destroy = "true"
}

// Set up deploy SSH key
resource "aws_key_pair" "unix_cascontroller_ssh_key" {
  key_name   = "${var.tenant}_${var.stack_name}_${var.app_environment}_cascontroller_ssh_key"
  public_key = "${file("/home/ec2-user/.ssh/deployer_key_rsa.pub")}"
}

// Create the VM instance
// You MUST have an instance profile that starts with your namespace attached to your instance
resource "aws_instance" "cascontroller" {
  iam_instance_profile   = "${var.tenant}-${var.stack_name}-${var.app_environment}-instance-profile"
  ami                    = "${data.aws_ami.unixeng_ami.id}"
  instance_type          = "${var.cascontroller_instance_type}"
  key_name               = "${aws_key_pair.unix_cascontroller_ssh_key.key_name}"
  subnet_id              = "${var.subnet_id}"
  vpc_security_group_ids = ["${var.security_group_id}", "${aws_security_group.unix_internal_sg.id}","${aws_security_group.sasviya_efs_sg.id}"]
  tags                   = "${merge(var.tags, map("Name", "${var.tenant}-${var.stack_name}-${var.app_environment}-cascontroller"))}"
  user_data              = "${data.template_cloudinit_config.cascontroller_final.rendered}"
  lifecycle {
    ignore_changes = ["ami", "iam_instance_profile", "instance_type", "key_name", "subnet_id", "user_data"]
  }
}

// Create a DNS record (optional)
resource "aws_route53_record" "cascontroller" {
  zone_id = "${var.zone_id}"
  name    = "ip-${replace(aws_instance.cascontroller.private_ip, ".", "-")}"
  type    = "A"
  ttl     = "1800"
  records = ["${aws_instance.cascontroller.private_ip}"]
}

// Outputs.tf
output "cascontroller_ipaddress" {
  value = "${aws_instance.cascontroller.private_ip}"
}

output "cascontroller_fqdn" {
  value = "${aws_route53_record.cascontroller.fqdn}"
}
